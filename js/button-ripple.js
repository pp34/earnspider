    const buttons = document.getElementsByClassName('btn');
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', function(e) {
            const wave = document.createElement('span');
            const waveY = e.offsetY,
                waveX = e.offsetX;
            wave.style.top = waveY + 'px',
                wave.style.left = waveX + 'px',
                wave.style.background = this.getAttribute('data-button-background');

            this.appendChild(wave);

            setTimeout(function() {
                wave.parentNode.removeChild(wave);
            }, 1500);
        });
    }