function init() {
    const sliders = document.getElementsByClassName("tick-slider-input");

    for (let slider of sliders) {
        slider.oninput = onSliderInput;
        updateValue(slider);
        updateLabels(slider);
        updateProgress(slider);
    }
}

function onSliderInput(event) {
    updateValue(event.target);
    paintTick(event.target);
    updateLabels(event.target);
    updateProgress(event.target);
}

function updateValue(slider) {
    let value = document.getElementById(slider.dataset.valueId);
    let correctValue = slider.value.toString();
    if (correctValue.indexOf('.') === -1) {
        correctValue += '.00';
    }
    value.innerHTML = "<div>" + correctValue + "</div>";
}

function updateLabels(slider) {
    const value = document.getElementById(slider.dataset.valueId);
    const minLabel = document.getElementById(slider.dataset.minLabelId);
    const maxLabel = document.getElementById(slider.dataset.maxLabelId);

    const valueRect = value.getBoundingClientRect();
    const minLabelRect = minLabel.getBoundingClientRect();
    const maxLabelRect = maxLabel.getBoundingClientRect();

    const minLabelDelta = valueRect.left - (minLabelRect.left);
    const maxLabelDelta = maxLabelRect.left - valueRect.left;

    const deltaThreshold = 32;

    if (minLabelDelta < deltaThreshold) minLabel.classList.add("hidden");
    else minLabel.classList.remove("hidden");

    if (maxLabelDelta < deltaThreshold) maxLabel.classList.add("hidden");
    else maxLabel.classList.remove("hidden");
}

function updateProgress(slider) {
    let progress = document.getElementById(slider.dataset.progressId);
    const percent = getSliderPercent(slider);

    progress.style.width = percent * 100 + "%";
}

function getSliderPercent(slider) {
    const range = slider.max - slider.min;
    const absValue = slider.value - slider.min;

    return absValue / range;
}

function paintTick(slider) {
    const tick = document.getElementsByClassName("js-tick-slider-label");
    let value = document.getElementById(slider.dataset.valueId);
    let newtick = tick[tick.length - 1];
    if (slider.value >= 100) {
        newtick.style.backgroundColor = "#43A4FF";
    } else {
        newtick.style.backgroundColor = "#6D6D6D";
    }
}

window.onload = init;