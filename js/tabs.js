'use strict';

function Tabs() {
    var bindAll = function() {
        var menuElements = document.querySelectorAll('[data-tab]');
        for (var i = 0; i < menuElements.length; i++) {
            menuElements[i].addEventListener('click', change, false);
        }
    }

    var clear = function() {
        var menuElements = document.querySelectorAll('[data-tab]');
        for (var i = 0; i < menuElements.length; i++) {
            menuElements[i].classList.remove('active');
            var id = menuElements[i].getAttribute('data-tab');
            document.getElementById(id).classList.remove('active');
        }
    }

    var change = function(e) {
        clear();
        e.target.classList.add('active');
        var id = e.currentTarget.getAttribute('data-tab');
        document.getElementById(id).classList.add('active');
    }

    bindAll();
}

var connectTabs = new Tabs();



$('#select__mob').on('select2:select', function(e) {
    console.log(e);
    document.querySelectorAll('.js-b-tab').forEach((n, i) => {
        n.classList.toggle('active', i === this.selectedIndex);
    });
});

$("#select__mob").select2({
    width: '100%',
    allowClear: true,
    minimumResultsForSearch: -1,
    templateResult: formatState,
    templateSelection: formatState,
});

function formatState(opt) {
    // console.log(opt);
    if (!opt.id) {
        return opt.text.toUpperCase();
    }

    var optimage = $(opt.element).attr('data-image');
    // console.log(optimage)
    if (!optimage) {
        return opt.text.toUpperCase();
    } else {
        var $opt = $(
            '<span><img src="' + optimage + '" width="40px" /> <p class="clip">' + opt.text + '</p></span>'
        );
        return $opt;
    }
};