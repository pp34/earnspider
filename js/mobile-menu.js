// In this code, I use my functions instead of the default ones ("getElementsByClassName", "hasClass", "addClass", "removeClass", "addEvent"), because older versions of browsers do not support them.
// But, perhaps, this does not make sense, because media queries start working from the IE 9, and I could use default "getElementsByClassName" and "addEvent".
(function() {
    var mainMenuContainer = document.getElementsByClassName('header__dropdown-container')[0];
    var burgerButtonContainer = document.getElementsByClassName('header__dropdown-burger')[0];
    var body = document.getElementsByClassName('page')[0];

    // Create a menu button, add classes, attributes, inner invisible text and add it to the page.
    burgerButtonContainer.innerHTML = '<button class="burger__button" aria-haspopup="true" aria-expanded="false" aria-label="Open menu"><span class="iconify" data-inline="false" data-icon="ic:baseline-menu" style="font-size: 24px;"></span></button>';

    function onBurgerClick() {
        if (burgerButton.getAttribute('aria-expanded') === 'true') { // If menu is already opened (check 'aria-expanded' attribute).
            burgerButton.setAttribute('aria-expanded', 'false');
            burgerButton.setAttribute('aria-label', 'Open menu');
            burgerButton.innerHTML = '<span class="iconify" data-inline="false" data-icon="ic:baseline-menu" style="font-size: 24px;"></span>';
            window.customFunction.removeClass(mainMenuContainer, 'header__dropdown-container--opened');
            window.customFunction.removeClass(body, 'fixed');
        } else {
            burgerButton.setAttribute('aria-expanded', 'true');
            burgerButton.setAttribute('aria-label', 'Close menu');
            burgerButton.innerHTML = '<span class="iconify" data-inline="false" data-icon="ic:baseline-close" style="font-size: 24px;"></span>';
            window.customFunction.addClass(mainMenuContainer, 'header__dropdown-container--opened');
            window.customFunction.addClass(body, 'fixed');
        }
    };
    var burgerButton = document.getElementsByClassName('burger__button')[0];
    window.customFunction.addEvent('click', burgerButton, onBurgerClick);
})();