tippy('#myButton_1', {
    content: 'Configure how much bandwidth to consume per day on your device (default in beta: 1GB maximum, over Wi-Fi)',
});
tippy('#myButton_2', {
    content: 'Display a table of all domains connected and their connection count so you can track the activity accordingly and determine your own comfortability',
});
tippy('#myButton_3', {
    content: 'Configure the number of connections allowed to go to a certain domain (the fewer connections, the less chances of harm being done but also the less earnings possible)',
});
tippy('#myButton_4', {
    content: 'Configure how much bandwidth to consume per day on your device (default in beta: 1GB maximum, over Wi-Fi)',
});